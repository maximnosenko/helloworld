/*
  main.cpp
  HelloWorld
*/

#include <iostream>

//This function prints string
void print()
{
    std::cout << "Hello Skillbox!\n";
}

int main()
{
    //Initialisation and multiplication of x and y
    int x = 100;
    int y = x + 100;
    int mult = x*y;
    
    int b = 0;
    b = b + 2;
    
    int test = 100500;
    int test2 = 1005001;
    
    int random = 1002;
    
    //Print all data
    std::cout << "Hello, World!\n";
    print();
    std::cout << "X = " << x << ", Y = " << y << ", X*Y = " << mult << '\n';
    std::cout << "b = " << b << ", test = " << test << ", test2 = " << test2 <<'\n';
    std::cout << "random = " << random << '\n';
}
